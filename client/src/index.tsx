// Copyright (C) 2020 Herobone & Aynril
// 
// This file is part of Lapislar.
// 
// Lapislar is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Lapislar is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Lapislar.  If not, see <http://www.gnu.org/licenses/>.

import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import LanguageContainer from './translations/LanguageContainer';
import { CookiesProvider } from 'react-cookie';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyAX_mPStrMno2OAM8K8HBpjwto-KbAYkW4",
  authDomain: "lapislar.firebaseapp.com",
  databaseURL: "https://lapislar.firebaseio.com",
  projectId: "lapislar",
  storageBucket: "lapislar.appspot.com",
  messagingSenderId: "267553799951",
  appId: "1:267553799951:web:ad5b1f5cb62a509a8b087a",
  measurementId: "G-VMZ4L3EMR4"
};
firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <React.StrictMode>
    <CookiesProvider>
        <LanguageContainer />
    </CookiesProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
