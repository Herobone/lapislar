import * as firebase from 'firebase/app';
import 'firebase/firestore';
import Util from './Util';
import * as Alerts from "../helper/AlertTypes";
import { ReactElement } from 'react';

class Tag {
    db: firebase.firestore.Firestore;
    user: firebase.User | null;
    tag: string | null | undefined;
    constructor(tag?: string) {
        this.db = firebase.firestore();
        this.user = firebase.auth().currentUser;
        this.tag = tag;
    }

    generate(): string {

        return Util.randomCharOrNumberSequence(10);

    }

    writeTag(callback: (tag: string | null, err?: string) => void, type?: string, tag?: string | null, tagNotExists?: boolean) {
        if (!tag) {
            tag = this.generate();
        }

        if (!type) {
            type = "normal"
        }

        if (!this.user) {
            return;
        }

        if (tagNotExists) {

            this.getDocRef(tag).get().then((doc: firebase.firestore.DocumentSnapshot) => {

                if (doc.exists) {
                    return this.writeTag(callback, type, this.generate(), false);
                } else {
                    return this.writeTag(callback, type, tag, true);
                }

            }).catch((err: firebase.FirebaseError) => {
                console.error(err);
                return callback(null, err.message);
            });

        } else {

            const data = {
                user: this.user.uid,
                date: firebase.firestore.FieldValue.serverTimestamp(),
                type: type
            }
            // noinspection JSIgnoredPromiseFromCall
            this.getDocRef(tag).set(data);
            return callback(tag);

        }
    }

    getDocRef(tag: string): firebase.firestore.DocumentReference {
        return this.db.collection("tags").doc(tag);
    }

    find(callback: (content: any | null, err?: string) => void): void {
        const tag = this.tag;
        if (!tag) {
            return;
        }

        this.getDocRef(tag).get().then((doc: firebase.firestore.DocumentSnapshot) => {

            if (doc.exists) {
                return callback(doc.data());
            } else {
                return callback(null);
            }

        }).catch((err: firebase.FirebaseError) => {
            console.error(err);
            return callback(null, err.message);
        });
    }

    iFoundThat(createAlert: (type: Alerts.Alert | number | string, message: string, header?: ReactElement | null) => void): void {
        if (!this.tag || !this.user) {
            return;
        }
        const data = {
            time: firebase.firestore.FieldValue.serverTimestamp(),
            method: "onSite"
        }
        this.db.collection("tags").doc(this.tag).collection("foundBy").doc(this.user.uid).set(data).catch((err: firebase.FirebaseError) => createAlert(3, err.message));
    }

}

export default Tag;