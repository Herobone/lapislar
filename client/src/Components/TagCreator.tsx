import React, {Component, ReactElement} from 'react'
import Column from './Column'
import {FormattedMessage} from 'react-intl';
import {Link} from 'react-router-dom';
import Tag from "../helper/Tag";
import * as Alerts from "../helper/AlertTypes";
import QRCode from "qrcode.react"
import Mascot from "../media/MascottAlpha.png"
import firebase from "firebase";

interface State {
    type: string;
    responseData: ResponseData | null;
    tag: string | null;
}

interface Props {
    user: firebase.User | null;
    createAlert: (type: Alerts.Alert | number | string, message: string, header?: ReactElement | null) => void;
}

interface ResponseData {
    test: string;
    num: number;
}

export class TagCreator extends Component<Props, State> {
    tabRefs: React.RefObject<HTMLButtonElement>[] = [];

    constructor(props: Props) {
        super(props);

        this.state = {
            type: "normal",
            responseData: null,
            tag: null
        }

        this.tabRefs.push(React.createRef());
        this.tabRefs.push(React.createRef());
        this.tabRefs.push(React.createRef())
        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    handleButtonClick() {
        const {type} = this.state;
        const tag = new Tag();
        if (this.state.tag !== null) {
            console.warn("Function called, but a tag was already generated!");
            this.props.createAlert(2, "Function called, but a tag was already generated!");
            return;
        }
        tag.writeTag((tag: string | null, err?: any) => {
            if (err || !tag) {
                this.props.createAlert(3, "Something went wrong! " + err);
                return;
            }
            this.setState({
                tag
            });
        }, type);

    }

    tabMap = [
        "normal",
        "code",
        "errorType"
    ]

    setTab(tab: number) {
        let cur = this.tabRefs[tab].current;
        const notCur: HTMLButtonElement[] = [];

        this.tabRefs.forEach(element => {
            if (element.current && cur !== element.current) {
                notCur.push(element.current)
            }
        });

        if (cur) {
            if (cur.className.indexOf("w3-theme-l4") === -1) {
                cur.className += " w3-theme-l4";
            }
        }

        for (let i = 0; i < notCur.length; i++) {
            const cur = notCur[i];
            cur.className = cur.className.replace(" w3-theme-l4", "");
        }

        this.setState({
            type: this.tabMap[tab]
        })
    }

    render() {
        const {user} = this.props,
            {type} = this.state;

        const imageSettings = {
            src: Mascot,
            height: 64,
            width: 64,
            excavate: true
        };

        return (
            <div className="tag-creator-component">
                {
                    user &&
                    <Column additionalClasses="w3-center">
                        <div>
                            <h3><FormattedMessage id="tags.create.type"/></h3>
                            <div className="w3-bar w3-theme">
                                <button ref={this.tabRefs[0]} className="w3-bar-item w3-button w3-xlarge"
                                        onClick={() => this.setTab(0)}><b>Normal</b></button>
                                <button ref={this.tabRefs[1]} className="w3-bar-item w3-button w3-xlarge"
                                        onClick={() => this.setTab(1)}><b>QR-Code</b></button>
                                <button ref={this.tabRefs[2]} className="w3-bar-item w3-button w3-xlarge"
                                        onClick={() => this.setTab(2)}><b>Error Type</b></button>
                            </div>
                            <hr/>
                            {
                                this.state.tag &&
                                <span>
                                    <h2><FormattedMessage id="general.done"/></h2>
                                </span>
                            }
                            {
                                type === "normal" &&
                                <div id="Normal">
                                    {
                                        this.state.tag &&
                                        <h3>
                                            <FormattedMessage
                                                id="tags.create.finished"
                                                values={{
                                                    tagID: this.state.tag
                                                }}/>
                                        </h3>
                                    }
                                </div>
                            }
                            {
                                type === "code" &&
                                <div id="Code">
                                    {
                                        this.state.tag &&
                                        <QRCode
                                            bgColor="#FFFFFF"
                                            fgColor="#000000"
                                            level="H"
                                            size={512}
                                            value={this.state.tag}
                                            renderAs="svg"
                                            imageSettings={imageSettings}
                                        />
                                    }
                                </div>
                            }
                        </div>
                        {!this.state.tag &&
                        <button type="submit"
                                onClick={this.handleButtonClick}
                                className="w3-button w3-round w3-xxxlarge w3-green">
                            <FormattedMessage id="tags.create.generate"/>
                        </button>
                        }
                        <br/>
                        <hr/>

                    </Column>
                }
                {
                    !user &&
                    <Column additionalClasses="w3-center">
                        <h3><FormattedMessage id="account.descriptors.notsignedin"/></h3>
                        <Link
                            to="/login"
                            className="w3-button w3-round w3-xlarge w3-green">
                            <FormattedMessage id="account.actions.login"/>
                        </Link>
                    </Column>
                }
            </div>
        )
    }
}

export default TagCreator
