import React, { Component, RefObject, ReactElement } from 'react'
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import Column from './Column';
import * as Alerts from "../helper/AlertTypes"
import Tag from '../helper/Tag';
import Util from '../helper/Util';

interface Props {
    user: firebase.User | null;
    createAlert: (type: Alerts.Alert | number | string, message: string | ReactElement, header?: ReactElement | null) => void;
}

interface State {

}

export class TagFound extends Component<Props, State> {
    tagIDRef!: RefObject<HTMLInputElement>;

    constructor(props: Props) {
        super(props);
        this.tagIDRef = React.createRef();
        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    handleButtonClick() {
        if (!this.tagIDRef) {
            return;
        }
        const cur = this.tagIDRef.current;
        if (!cur) {
            return;
        }
        if (cur.value.length !== 10) {
            return this.props.createAlert(2, <FormattedMessage id="tags.found.invalidlen.body" />, <FormattedMessage id="tags.found.invalidlen.header" />);
        }
        if (!Util.alphanumeric(cur.value)) {
            return this.props.createAlert(2, <FormattedMessage id="tags.found.onlyalphanum.body" />, <FormattedMessage id="tags.found.onlyalphanum.header" />); 
        }
        const tag = new Tag(cur.value);
        tag.find((content: any, err?: string) => {
            if (!err) {
                if (content) {
                    this.setState({
                        cont: content
                    });
                    return tag.iFoundThat(this.props.createAlert);
                } else {
                    return this.props.createAlert(2, <FormattedMessage id="tags.notfound" />);
                }
            } else {
                return this.props.createAlert(3, "Unknown Error: " + err);
            }
        })
    }

    render() {
        const { user } = this.props;
        return (
            <div className="tag-found-component">
                {
                    user &&
                    <Column additionalClasses="w3-center">
                        <div className="w3-center">
                            <h1><FormattedMessage id="tags.found.enteratag" /></h1>
                            <span className="w3-input w3-border-0 w3-xxlarge">
                                #
                                <input
                                    ref={this.tagIDRef}
                                    className="id-input center-input"
                                    type="text"
                                    placeholder="Tag ID"
                                    autoComplete="off"
                                    spellCheck={false}
                                    maxLength={10} />
                            </span>
                            <br />
                            <button type="submit"
                                onClick={this.handleButtonClick}
                                className="w3-button w3-round w3-xxxlarge w3-indigo w3-padding-16"
                                style={{ "width": "20vw" }}>
                                <FormattedMessage id="tags.found.go" />
                            </button>
                        </div>
                        <br />

                        <div>
                        
                        </div>
                    </Column>
                }
                {
                    !user &&
                    <Column additionalClasses="w3-center">
                        <h3><FormattedMessage id="account.descriptors.notsignedin" /></h3>
                        <Link
                            to="/login"
                            className="w3-button w3-round w3-xlarge w3-green">
                            <FormattedMessage id="account.actions.login" />
                        </Link>
                    </Column>
                }
            </div>
        )
    }
}

export default TagFound
