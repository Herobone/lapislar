module herobone.de/lapislar

go 1.14

require (
	cloud.google.com/go/firestore v1.2.0 // indirect
	firebase.google.com/go v3.12.1+incompatible
	github.com/GoogleCloudPlatform/functions-framework-go v1.0.1
	github.com/golang/gddo v0.0.0-20200324184333-3c2cc9a6329d
	google.golang.org/api v0.20.0
)
