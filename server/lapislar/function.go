// Package helloworld provides a set of Cloud Functions samples.
package lapislar

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"log"
	"net/http"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

func initFirestore() context.Context {
	// Use the application default credentials
	ctx := context.Background()
	sa := option.WithCredentialsFile("path/to/serviceAccount.json")
	app, err := firebase.NewApp(ctx, nil, sa)
	if err != nil {
		conf := &firebase.Config{ProjectID: "lapislar"}
		app, err = firebase.NewApp(ctx, conf)
		if err != nil {
			log.Fatalln(err)
		}
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln(err)
	}
	defer client.Close()
	return ctx
}

type Person struct {
	Name string
	Age  int
}

func PersonCreate(w http.ResponseWriter, r *http.Request) {
	var p Person

	err := decodeJSONBody(w, r, &p)

	keys, ok := r.URL.Query()["key"]

	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param 'key' is missing")
	} else {
		key := keys[0]
		log.Println("Url Param 'key' is: " + string(key))
	}

	if err != nil {
		var mr *malformedRequest
		if errors.As(err, &mr) {
			m := map[string]string{"msg": mr.msg}
			jsonResponse(w, m, mr.status)
		} else {
			log.Println(err.Error())
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		return
	}

	jsonResponse(w, p, http.StatusOK)

}

// HelloHTTP is an HTTP Cloud Function with a request parameter.
func HelloHTTP(w http.ResponseWriter, r *http.Request) {
	var d struct {
		Name string `json:"name"`
	}
	fmt.Println(w, r.Body)
	if err := json.NewDecoder(r.Body).Decode(&d); err != nil {
		fmt.Fprint(w, "Hello, Error!")
		return
	}
	if d.Name == "" {
		fmt.Fprint(w, "Hello, World!")
		return
	}
	fmt.Fprintf(w, "Hello, %s!", html.EscapeString(d.Name))
}
